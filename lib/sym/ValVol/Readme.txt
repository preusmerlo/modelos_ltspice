My library models for the simulator LTspice. The archive contains the model:
1. PWM controller TL494.
2. PWM controller UC3825
3. PWM controller SG3525A
4. High Voltage Dual Interleaved Current Mode Controller LM5032
5. Synchronous Buck Regulator LM5017
6. Integral FlyBack Converters TNY267 and TNY280
7. Resonant Mode Controller MC33067
8. Resonant Mode Controller UCC25600
9. Resonant Mode Controller FAN7621
10.Resonant Mode Controller FAN7688
11.CMOS PLL Controllers 74HC4046
12.Phase Shift Resonant Controller UC3875
13.Phase Shift Resonant Controller UC3879
14.Flyback Controller UCC28610
15.PFC controller UC2854
16.PFC controller UCC28070
17.Driver Optocoupler HCPL3180
18.Optocoupler driver HCPL316
19.Triac driver optocoupler MOC3082
20.Triac driver optocoupler MOC3052
21.The model of the operational amplifier K544UD2 (borrowed from the library r-opamp.lib Micro-Cap)
22.G_loop module for measuring the hysteresis loop of a nonlinear inductance
23.Winding of ideal transformer

Unzip the archive to a folder sym / ValVol.
Feedback and suggestions can be sent to email:
valvolodin@narod.ru or valvol@inbox.ru

--
Best regards,
Valentyn Volodin

��� ���������� ������� ��� ���������� LTspice. � ������ ��������� ������:
1. ��� ���������� TL494 
2. ��� ���������� UC3825
3. ��� ���������� SG3525A
4. �������������� ���������� ��������� ���������� �������� ���������� LM5032
5. ���������� ���������� ��������������� LM5017
6. ������������ �������������� ��������������� TNY267 � TNY280
7. ����������� ���������� MC33067
8. ����������� ���������� UCC25600
9. ����������� ���������� FAN7621
10.����������� ���������� FAN7688
11.���� ����������� ���� 74HC4046
12.�������������� ����������� ���������� UC3875
13.�������������� ����������� ���������� UC3879
14.���������� ��������������� ��������������� UCC28610
15.���������� ��� UC2854
16.���������� ��� UCC28070
17.��������� ������� HCPL3180
18.��������� ������� HCPL316
19.��������� ����������� ������� MOC3082
20.��������� ����������� ������� MOC3052
21.������ ������������� ��������� �544��2 (�������������� �� ���������� r-opamp.lib Micro-Cap)
22.������ G_loop ��� ������ ����� ����������� ���������� ������������� 
23.������� ���������� ��������������

����� ���������� ��������������� � ����� sym/ValVol. 
��������� � ����������� ����� ���������� �� ������:
valvolodin@narod.ru ��� valvol@inbox.ru

--
� ���������,
�������� �������