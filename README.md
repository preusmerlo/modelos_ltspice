# Modelos SPCIE para LTSpice XVII
Este repositorio contiene un gran conjunto de modelos  de componentes electrónicos ya   implementados y listos para usarse. Los modelos no son de mi autoría, provienen de muchos  foros, grupos y  blogs de internet. No todos están probados.
En la siguiente guía se asume que LTSpice XVII ya ha sido instalado. Si esto no es así, puede descargar este software haciendo click [aqui](https://www.analog.com/en/design-center/design-tools-and-calculators/ltspice-simulator.html). 

## Instalación
Para clonar este repositorio usando Windows es necesario tener instalado "git bash for windows" que puede descargarse haciendo click [aqui](https://gitforwindows.org/). Luego de instalado el programa, abrir el directorio en el que se desea clonar el respositorio, hacer click izquierdo y seleccionar la opción "Git Bash Here". Se abrirá una consola al mejor estilo Linux y en ella se debe ingresar el siguiente comando: 

```
git clone https://gitlab.com/preusmerlo/modelos_ltspice.git
``` 

En el directorio se descargará una carpeta con el nombre "modelos_ltspice" que en su interior contiene una única carpeta llamada "lib" y un archivo pdf. **Copie** la  carpeta "lib". No el contenido, la carpeta. Luego, debe dirigirse al directorio:

```
\Documentos\LTspiceXVII
``` 
Aquí debe **pegar** y **reemplazar** la carpeta "lib". Todos los componentes ya están disponibles para usarse. La proxima vez que ingrese a LTSpice al persionar la tecla F2 una nueva carpeta llamada "[ZZZ]" aparecerá. Allí se encuentran todos los componentes instalados.